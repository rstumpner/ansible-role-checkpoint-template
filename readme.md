# Ansible Role Checkpoint Configuration Template

This is a Ansible Template Role for CLISH configuration on Checkpoint Devices.

Aufbau:
- Build Checkpoint Config
  - All Availiable Build Modules
    - All ansible modes
    - tags: build
  - Cisco Checkpoint CLISH
    - All ansible modes
    - tags: 
      - build-cli
      - cli

Requirements:
    None

Role Variables:
    - hostname

Using this Role:
Drive to Ansible Role Directory:
    - git clone https://gitlab.com/rstumpner/ansible-role-checkpoint-template

Aktivate Role in a Playbook:

Example:
```YAML
- hosts: all
  roles:
     - ansible-role-checkpoint-template
```

Tested:
 - Cisco IOS
 - Cisco IOS-XE
 
License:
    MIT / BSD

Author Information:
roland@stumpner.at